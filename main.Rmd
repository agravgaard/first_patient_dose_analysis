---
title: "Dose analysis"
author: "Andreas Gravgaard Andersen"
date: "17/11/2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)

if (!is.element("remotes", installed.packages()[, 1])) {
  install.packages("remotes")
}
library(remotes)

if (!is.element("tidyverse", installed.packages()[, 1])) {
  install.packages("tidyverse")
}
library(tidyverse)
```

# Gamma pass rates

Load data

```{r, label="loading"}
cb_folder <- "CB1"
df <-
  read_tsv(paste(cb_folder, "GPR_rep_vs_x_results.txt", sep = "/"))
```

Reorder data and make it convenient for ggplot'ing

```{r, label="reorder"}
df <- df %>%
  mutate(Tolerance = paste0(Percent, "%/", Distance, "mm")) %>%
  mutate(across(where(is_character), as.factor)) %>%
  mutate(CT = reorder(CT, Passrate, mean))
```

OK, let's see

```{r, label="FirstPlot"}
df %>%
  ggplot(aes(CT, Passrate, color = Tolerance)) +
  theme_bw(16) +
  geom_point(size = 5)

```

Interesting

```{r, label="AsPercent"}
df %>%
  group_by(CT, Tolerance) %>%
  mutate(RelativePassrate = 
           Passrate / df[df$CT == "def" & df$Tolerance == Tolerance,]$Passrate) %>%
  ggplot(aes(CT, RelativePassrate, color = Tolerance)) +
  theme_bw(16) +
  geom_point(size = 5)

```

Not really better....


# DVH

Load data

```{r, label="loadDVH"}
CTs <- c("raw", "cor", "def", "rig", "rep")
my_load_csv <- function(ct_name){
  read.csv(paste(cb_folder, paste("dvh", ct_name, "csv", sep = "."), sep = "/"))
}
dvh_list <- lapply(CTs, my_load_csv)
```

Reorder for convenience

```{r, label="reorder.reborn"}
dvh_df <- dvh_list %>%
  bind_rows(.id = "column_names") %>%
  mutate(CT = CTs[as.integer(column_names)]) %>%
  pivot_longer(cols = -c("Dose..Gy." , "column_names", "CT"), names_to = "Structure") %>%
  filter(!str_detect(Structure, "o\\.")) %>%
  filter(!str_detect(Structure, "^(Eye|Hippo|Lens|Beek|Match)")) %>%
  mutate(Dose_Gy = Dose..Gy. * 6.8) # 10 -> 68

rep_only <- dvh_df[dvh_df$CT == "rep",]

dvh_df <- dvh_df %>%
  left_join(select(rep_only, Structure, Dose_Gy, RefValue = value), by = c("Structure", "Dose_Gy")) %>%
  mutate(RefDifference = RefValue - value) %>%
  mutate(RelativeDifference = RefDifference / RefValue)

remove(rep_only)
```

Lets see

```{r}
ctv_only <- dvh_df %>%
  group_by(CT, Structure) %>%
  filter(str_detect(Structure, "^CTV.*Gy")) %>%
  filter(Dose..Gy. < 12.0)
  
ctv_plot <- ctv_only %>%
  ggplot(aes(x = Dose_Gy , y = value, colour = CT)) +
    theme_bw() +
    scale_colour_viridis_d() +
    geom_line() + 
    facet_wrap(~Structure)

ctv_plot
```

```{r}
tol_dose <- .95
tol_vol <- .95
ctv_plot +
  coord_cartesian(xlim = c(40,70), ylim = c(.9, 1.05)) +
  geom_vline(data = filter(ctv_only, Structure == "CTV1_68Gy1"), aes(xintercept = 68 * tol_dose)) +
  geom_vline(data = filter(ctv_only, Structure == "CTV2_60Gy"), aes(xintercept = 60 * tol_dose)) +
  geom_vline(data = filter(ctv_only, Structure == "CTV3_50Gy"), aes(xintercept = 50 * tol_dose)) +
  geom_hline(yintercept = tol_vol)
```

Why are we still here? Just to suffer?

```{r, label="FindOARs"}
to_exclude <- c("BODY", "Skin_3mm", "Skin_7mm")

oar_only <- dvh_df %>%
  group_by(CT, Structure) %>%
  filter(!str_detect(Structure, "(G|C|R)TV")) %>%
  filter(!(Structure %in% to_exclude)) %>%
  filter(Dose..Gy. < 12.0)
  
oar_only %>%
  ggplot(aes(x = Dose_Gy , y = value, colour = CT)) +
    theme_bw() +
    scale_colour_viridis_d() +
    geom_line() + 
    facet_wrap(~Structure)
```

Let's look at the ones with the biggest differences

```{r, label="CherryPick"}
more_interesting <- c("Esophagus", "OralCavity", "PCM_Up")

oar_only %>%
  filter(Structure %in% more_interesting) %>%
  ggplot(aes(x = Dose_Gy , y = value, colour = CT)) +
    theme_bw() +
    scale_colour_viridis_d() +
    geom_line() + 
    facet_wrap(~Structure)
```

Meh... the correction is almost doing the worst here.

Relative differences?

```{r, label="RelDiff"}
to_exclude <- c(to_exclude, "BrainStem", "Cochlea_L", "Lips", "Parotid_R")

dvh_df %>%
  filter(CT != "rep") %>%
  group_by(Structure) %>%
  filter(!str_detect(Structure, "(G|C|R)TV")) %>%
  filter(!(Structure %in% to_exclude) &
           Dose..Gy. < 12.0 &
           RefValue > 0 &
           !is.na(RefDifference) &
           !is.infinite(RefDifference) &
           !is.nan(RefDifference) &
           RefDifference > 0.001) %>%
  ggplot(aes(x = Dose_Gy , y = RefDifference, colour = CT)) +
    theme_bw(16) +
    scale_colour_viridis_d() +
    geom_line() + 
    facet_wrap(~Structure) +
    coord_cartesian(ylim = c(-.3, .3), xlim = c(0, 70))
```

```{r, label="cherryPickOAR"}
dvh_df %>%
  filter(CT != "rep") %>%
  filter(Structure %in% more_interesting) %>%
  ggplot(aes(x = Dose_Gy , y = RefDifference, colour = CT)) +
    theme_bw(16) +
    scale_colour_viridis_d() +
    geom_line() + 
    facet_wrap(~Structure) +
    coord_cartesian(ylim = c(-.3, .3), xlim = c(0, 70))
```

```{r, label="cherryPickCTV"}
dvh_df %>%
  filter(CT != "rep") %>%
  filter(str_detect(Structure, "^CTV.*Gy")) %>%
  filter(abs(RefDifference) > 0 &
           Dose_Gy > 40) %>%
  ggplot(aes(x = Dose_Gy , y = RefDifference, colour = CT)) +
    theme_bw(16) +
    scale_colour_viridis_d() +
    geom_line() + 
    facet_wrap(~Structure, scales = "free_x") +
    coord_cartesian(ylim = c(-.05, .26))
```

Hmm, maybe there is a tiny hope, let's magnify it

```{r, label="SuperCherryPick"}

dvh_df %>%
  filter(!(CT %in% c("rep", "raw"))) %>%
  filter(str_detect(Structure, "^CTV.*Gy")) %>%
  filter(abs(RefDifference) > 0 &
           Dose_Gy > 40) %>%
  ggplot(aes(x = Dose_Gy , y = abs(RefDifference), colour = CT)) +
    theme_bw(16) +
    scale_colour_viridis_d() +
    geom_line() + 
    facet_wrap(~Structure, scales = "free")

```


## Quantify DVH

```{r}
oar_only %>%
  group_by(Structure, CT) %>%
  summarise(AvgDiff = mean(abs(RefDifference))) %>%
  arrange(desc(AvgDiff))
  
oar_only %>%
  group_by(Structure, CT) %>%
  summarise(AvgAbsDiff = mean(abs(RefDifference))) %>%
  group_by(CT) %>%
  summarise(SumAvgAbsDiff = sum(AvgAbsDiff)) %>%
  arrange(SumAvgAbsDiff)


ctv_only %>%
  group_by(Structure, CT) %>%
  summarise(AvgDiff = mean(abs(RefDifference))) %>%
  arrange(desc(AvgDiff))

ctv_only %>%
  group_by(Structure, CT) %>%
  summarise(AvgAbsDiff = mean(abs(RefDifference))) %>%
  group_by(CT) %>%
  summarise(SumAvgAbsDiff = sum(AvgAbsDiff)) %>%
  arrange(SumAvgAbsDiff)
```

Welp, too bad rCT was uneccessary